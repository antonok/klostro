# klostro

klostro is a tiny, monospaced bitmap font specialized for use on low-resolution screens. With each character tuned to fit on a 5x8 pixel grid, this font is designed to squeeze the most content out of hardware that isn't meant to display it.

klostro covers the full 256-character extended ASCII range with reasonable clarity given its size constraints.

## preview

![klostro's entire 256-character extended ASCII range](klostro_5x8.png)

## source?

You may be asking, "where are the source files for klostro?"

In this case, the PNG file included in this repo is the source. Each character was hand-crafted pixel-by-pixel in [GIMP](https://www.gimp.org).

## font file?

You may also be asking, "how can I use this font?"

klostro was designed to be used for hardware, primarily on FPGAs. The tool `mkfontrom` can be used to transform this font into a hardware-synthesizable memory initializer that can be read using Verilog's `$readmemb` directive. For further information, be sure to check out the [`mkfontrom` repository](https://www.gitlab.com/antonok/mkfontrom).
